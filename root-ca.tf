resource "vault_mount" "kube-root-ca" {
  path = "kube-root-ca-${var.clusterName}"
  type = "pki"
  default_lease_ttl_seconds = 3153600000
  max_lease_ttl_seconds = 3153600000
}

resource "vault_pki_secret_backend_root_cert" "root-ca" {
  backend = vault_mount.kube-root-ca.path
  type = "internal"
  common_name = "kube-root-ca"
  ttl = "315360000"
  format = "pem"
  private_key_format = "der"
  key_type = "rsa"
  key_bits = 4096
  exclude_cn_from_sans = true
  ou = "Kubernetes Lab"
  organization = "Kubernetes Lab"
}

variable "templateImagePath" {
  type = string
  description = "Path to template image on filesystem."
}

variable "baseVmDataPoolDir" {
  type = string
  default = "/var/lib/libvirt/kube-data-pools"
  description = "Path to VM data pool."
}

variable "networkName" {
  type = string
  default = "default"
  description = "VM network name."
}

variable "vmNetwork" {
  type = string
  default = "192.168.122.0/24"
}

variable "netMask" {
  type = string
  default = "255.255.255.0"
}

variable "netGateway" {
  type = string
  default = "192.168.122.1"
}

variable "DNS1" {
  type = string
  default = "8.8.8.8"
}

variable "DNS2" {
  type = string
  default = "8.8.4.4"
}

variable "startNetAddress" {
  type = number
  default = 10
}

variable "workerVmNamePattern" {
  type = string
  default = "kube-node"
  description = "Name pattern for workers VM."
}

variable "workersNumber" {
  type =  number
  default = 2
}

variable "workerVmConfiguration" {
  type = object({
    ram = number,
    vCpu = number
  })
  default = {
    ram = 4096,
    vCpu = 4
  }
}

variable "masterVmConfiguration" {
  type = object({
    vmName = string,
    ram = string,
    vCpu = number,
    ipAddr = string
  })
  default = {
    vmName = "kube-master-01",
    ram = 4096,
    vCpu = 2,
    ipAddr = "192.168.122.200"
  }
}

variable "corePasswdHash" {
  type =  string
  description = "Password  hash for user - core."
}

variable "sshPubKey" {
  type = string
  description = "SSH public key for core user."
}

variable "domain" {
  type = string
  default = "local"
}

variable "clusterName" {
  type = string
  default = "kube-cluster"
}

variable "kubernetesCaRoleName" {
  type = string
  default = "kubernetes-ca"
}

variable "kubernetesCaOrganizations" {
  type = list(string)
  default = ["Kubernetes Lab"]
}

variable "caOrganization" {
  type = string
  default = "Kubernetes Lab"
}

variable "caProvince" {
  type = list(string)
  default = [
    "MSK"
  ]
}



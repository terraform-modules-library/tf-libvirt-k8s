
resource "libvirt_volume" "masterVmDataVolume" {
  name   = var.masterVmConfiguration.vmName
  pool   = libvirt_pool.vmData.name
  source = var.templateImagePath
  format = "qcow2"
}

data "template_file" "master-ign-template" {
  template = file("${path.module}/master-ign.yaml")
  vars =  {
    coreUserPasswordHash = var.corePasswdHash
    sshPubKey = var.sshPubKey
    addr = var.masterVmConfiguration.ipAddr
    mask = cidrnetmask(var.vmNetwork)
    gateway = var.netGateway
    dns1 = var.DNS1
    dns2 = var.DNS2
    hostname = var.masterVmConfiguration.vmName
    domain = var.domain
    rootCAcert = vault_pki_secret_backend_root_sign_intermediate.kubernetes-ca-sign.certificate
    rootCAkey = vault_pki_secret_backend_intermediate_cert_request.kubernetes-ca-request.private_key
  }
}

data "ct_config" "master-ignition-userdata" {
  content = data.template_file.master-ign-template.rendered
  pretty_print = true
}

resource "libvirt_ignition" "master-ignition" {
  name    = "${var.masterVmConfiguration.vmName}-ignition"
  pool    = libvirt_pool.vmData.name
  content = data.ct_config.master-ignition-userdata.rendered
}

resource "libvirt_domain" "masterVmNode" {
  name   = var.masterVmConfiguration.vmName
  memory = var.masterVmConfiguration.ram
  vcpu   = var.masterVmConfiguration.vCpu
  coreos_ignition =  libvirt_ignition.master-ignition.id
  network_interface {
    network_name = var.networkName
  }

  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  disk {
    volume_id = libvirt_volume.masterVmDataVolume.id
  }

  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = true
  }
}


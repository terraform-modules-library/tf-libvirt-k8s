#!/usr/bin/env bash

if [[ ! -f /var/lib/kubelet/config.yaml ]]; then
	printf "Kubelet configuration file does't exists, so try bootstrap  node to join in the cluster...\n"
fi

rc=1
while [[ rc -ne 0 ]]
do
	sleep 5
	printf "Try connect to kubernetes api server...\n"
	kubectl get cs
	rc=$?

done

printf "Seems to api server is reachable and its healt is OK!\n"
printf "Join node %s to kubernetes cluster...\n"  $(hostname)
kubeadm join --discovery-file=${KUBECONFIG}

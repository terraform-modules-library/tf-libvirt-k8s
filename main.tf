provider "libvirt" {
  uri = "qemu:///system"
}

provider "vault" {

}

resource "libvirt_pool" "vmData" {
  name = var.clusterName
  type = "dir"
  path = "${var.baseVmDataPoolDir}/${var.clusterName}"
}
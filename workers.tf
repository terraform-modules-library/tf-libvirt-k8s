
resource "libvirt_volume" "workerVmDataVolume" {
  count = var.workersNumber
  name   = "${var.workerVmNamePattern}-${count.index + 1}"
  pool   = libvirt_pool.vmData.name
  source = var.templateImagePath
  format = "qcow2"
}


resource "vault_pki_secret_backend_cert" "bootstrap-node-keys" {
  count = var.workersNumber
  backend = vault_mount.kubernetes-ca.path
  common_name = "system:master:${var.workerVmNamePattern}-${count.index + 1}"
  name = vault_pki_secret_backend_role.kube-cluster-pki-node-join.name
}

data "template_file" "worker-user-data" {
  count = var.workersNumber
  template = file("${path.module}/workers-ign.yaml")
  vars =  {
    coreUserPasswordHash = var.corePasswdHash
    sshPubKey = var.sshPubKey
    addr = cidrhost(var.vmNetwork, var.startNetAddress + count.index)
    mask = cidrnetmask(var.vmNetwork)
    gateway = var.netGateway
    dns1 = var.DNS1
    dns2 = var.DNS2
    hostname = "${var.workerVmNamePattern}-${count.index + 1}"
    domain = var.domain
    bootstrapCert = vault_pki_secret_backend_cert.bootstrap-node-keys[count.index].certificate
    bootstrapKey = vault_pki_secret_backend_cert.bootstrap-node-keys[count.index].private_key
    rootCAcertB64 = base64encode(vault_pki_secret_backend_root_sign_intermediate.kubernetes-ca-sign.certificate)
    masterIPaddr = var.masterVmConfiguration.ipAddr
  }
}

data "ct_config"  "worker-ign-user-data" {
    count = var.workersNumber
    content = data.template_file.worker-user-data[count.index].rendered
    pretty_print = true
}

resource "libvirt_ignition" "worker-ignition" {
  count =  var.workersNumber
  name    = "${var.workerVmNamePattern}-${count.index + 1}-ignition"
  pool    = libvirt_pool.vmData.name
  content = data.ct_config.worker-ign-user-data[count.index].rendered
}


resource "libvirt_domain" "workerVmNode" {
  count = var.workersNumber
  name   = "${var.workerVmNamePattern}-${count.index + 1}"
  memory = var.workerVmConfiguration.ram
  vcpu   = var.workerVmConfiguration.vCpu
  coreos_ignition = libvirt_ignition.worker-ignition[count.index].id
  network_interface {
    network_name = var.networkName
  }

  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  disk {
    volume_id = libvirt_volume.workerVmDataVolume[count.index].id
  }

  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = true
  }
}


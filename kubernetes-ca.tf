resource "vault_mount" "kubernetes-ca" {
  path = "kubernetes-ca-${var.clusterName}"
  default_lease_ttl_seconds = 3153600000
  max_lease_ttl_seconds = 3153600000
  type = "pki"
}

resource "vault_pki_secret_backend_intermediate_cert_request" "kubernetes-ca-request" {
  backend = vault_mount.kubernetes-ca.path
  common_name = var.clusterName
  type = "exported"
  format = "pem"
  key_type = "rsa"
  key_bits = 4096
}

resource "vault_pki_secret_backend_root_sign_intermediate" "kubernetes-ca-sign" {
  backend = vault_mount.kube-root-ca.path
  common_name = vault_pki_secret_backend_intermediate_cert_request.kubernetes-ca-request.common_name
  csr = vault_pki_secret_backend_intermediate_cert_request.kubernetes-ca-request.csr
  organization = var.caOrganization
  ou = var.clusterName
}

resource "vault_pki_secret_backend_intermediate_set_signed" "kubernetes-ca-intermediate" {
  backend = vault_mount.kubernetes-ca.path
  certificate =  vault_pki_secret_backend_root_sign_intermediate.kubernetes-ca-sign.certificate
}

resource "vault_pki_secret_backend_role" "kube-cluster-pki-node-join" {
  backend = vault_mount.kubernetes-ca.path
  name = "node-join"
  allow_any_name = true
  organization = ["system:masters"]
  province = var.caProvince
  key_bits = 4096
  key_type = "rsa"
  allowed_domains = ["*"]
  client_flag = true
  server_flag = false
  allow_bare_domains = true
  allow_glob_domains = true
  allow_subdomains = true
  ttl = "300s"
  max_ttl = "300s"
  enforce_hostnames = false
}


//resource "vault_mount" "kubernetes-clusters-ca" {
//  path = "kubernetes-clusters-ca-keys"
//  type = "generic"
//  description = "Storage of certificate pair for many kubernetes clusters"
//}
//
//
//resource "vault_generic_secret" "ca-key-pair" {
//  data_json = jsonencode({"cert": vault_pki_secret_backend_root_sign_intermediate.ca-cert-sign.certificate, "key": vault_pki_secret_backend_intermediate_cert_request.ca-cert-request.private_key })
//  path = "/kubernetes-clusters-ca-keys/${var.clusterName}/kubernetes"
//}




